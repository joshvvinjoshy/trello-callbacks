const boards = require('../../Downloads/boards.json');
const callback3 = require('./callback3.cjs');
const callback2 = require('./callback2.cjs');
function callback6(name, cb){
    setTimeout( () =>{
        const board = boards.find(function (board){
            if(board.name == 'Thanos'){
                return true;
            }
        })    
        if(board == undefined){
            console.error(new Error("There is no name with Thanos in boards.json"));
        }
        else{
            callback2(board.id, (err,lists) =>{
                if(err){
                    console.error(err);
                }
                else{
                    Object.entries(lists).map(function (list){
                        callback3(list[1].id, (err,cards) =>{
                            if(err){
                                console.error(err);
                            }
                            else{
                                if(cards != undefined){
                                    console.log(cards);
                                }
                            }
                        })
                    });
                }
            });            
        }
    }, 2*1000);
}
module.exports = callback6;