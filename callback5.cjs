const boards = require('../../Downloads/boards.json');
const callback3 = require('./callback3.cjs');
const callback2 = require('./callback2.cjs');
function callback5(){
    setTimeout( () =>{

        const board = boards.find(function (board){
            if(board.name == 'Thanos'){
                return true;
            }
        })    
        if(board == undefined){
            console.error(new Error("There is no name with Thanos in boards.json"));
        }
        else{
            callback2(board.id, (err,lists) =>{
                if(err){
                    console.error(err);
                }
                else{
                    // console.log(lists);
                    Object.entries(lists).map(function (list){
                        // console.log(list[1]);
                        if(list[1].name == 'Mind' || list[1].name == 'Space'){
                            callback3(list[1].id, (err,cards) =>{
                                if(err){
                                    console.error(err);
                                }
                                else{
                                    if(cards != undefined){
                                        console.log(cards);
                                    }
                                }
                            })
                        }
                    });
                }
            });            
        }
    }, 2*1000);
}
module.exports = callback5;