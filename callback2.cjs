const lists = require('../../Downloads/lists.json');
function callback2(boardid, cb){
    setTimeout(() =>{
        if(boardid == undefined){
            cb(new Error("boardid is undefined"));
        }
        else if(boardid == ''){
            cb(new Error("boardid is null"));
        }
        else{
            let answer = lists[boardid];
            // console.log(board);
            if(answer == undefined){
                console.log("There is no list with", boardid, "in lists.json");
            }
            else{
                cb(null,answer);        
            }
        }
    }, 2*1000);
}
module.exports = callback2;