const boards = require('../../Downloads/boards.json');
function callback1(boardid, cb){
    setTimeout(() =>{
        if(boardid == undefined){
            cb(new Error("boardid is undefined"))
        }
        else if(boardid == ''){
            cb(new Error("boardid is null"));
        }
        else{
            const board = boards.find((board) =>{
                if(board.id == boardid){
                    return true;
                }
            })
            // console.log(board);
            if(board == undefined){
                console.log("There is no board with", boardid, "in boards.json");
            }
            else{
                cb(null, board);            
            }
        }
    }, 2*1000);
}
module.exports = callback1;