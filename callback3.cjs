const cards = require('../../Downloads/cards.json');
function callback3(listid, cb){
    setTimeout( () =>{

        if(listid == undefined){
            cb(new Error("boardid is undefined"));
        }
        else if(listid == ''){
            cb(new Error("boardid is null"));
        }
        else{
            const answer = cards[listid];
            if(answer == undefined){
                console.log("There is no list with", listid, "in cards.json");
            }
            else{
                cb(null,answer);        
            }
        }
    },2*1000);
}
module.exports = callback3;